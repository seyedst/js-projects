let reversedNum = (num) => (
  parseFloat(num.toString().split('').reverse().join('')) * Math.sign(num)
);

console.log(reversedNum(123))
console.log(reversedNum(-123))
console.log(reversedNum(120))
console.log(reversedNum(0))
