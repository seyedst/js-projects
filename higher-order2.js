function loop(start, isTrue, newNum, body) {
  
  for (let value = start; isTrue(value); value = newNum(value)) {
    
    body(value);
  }
}

loop(10, n => n > 0, n => n - 1, console.log);